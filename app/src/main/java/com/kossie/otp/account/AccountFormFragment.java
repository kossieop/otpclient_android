package com.kossie.otp.account;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.kossie.otp.OtpApp;
import com.kossie.otp.R;
import com.kossie.otp.model.OtpAccount;
import com.kossie.otp.util.AccountUtil;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by sunghun on 6/09/15.
 */
public class AccountFormFragment extends Fragment {

    private AccountUtil mAccountUtil;

    @Bind(R.id.edit_account_name)
    EditText mEditAccountName;
    @Bind(R.id.edit_account_key)
    EditText mEditAccountKey;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAccountUtil = ((OtpApp)getActivity().getApplication()).component().provideAccountUtil();
        mAccountUtil.getAccountsFromPreference();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account_form, container, false);
        ButterKnife.bind(this, view);

        initializeView(view);
        return view;
    }

    /**
     *
     * @param view
     */
    private void initializeView(View view) {

    }

    /**
     *
     * @param view
     */
    @OnClick(R.id.btn_save_changes)
    void btnSaveClick(View view){

        OtpAccount account = new OtpAccount(mEditAccountName.getText().toString(), mEditAccountKey.getText().toString());
        if (mAccountUtil.exist(account)) {
            return;
        }

        mAccountUtil.add(account);
        Snackbar.make(view, "Account saved", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

}
