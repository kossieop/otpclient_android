package com.kossie.otp.inject;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.kossie.otp.OtpApp;
import com.kossie.otp.util.AccountUtil;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by sunghun on 20/09/15.
 */

@Singleton
@Component(modules = {AndroidModule.class})
public interface ApplicationComponent {

    public void inject(OtpApp otpApp);

    public SharedPreferences providePreference();

    public Gson provideGson();

    public AccountUtil provideAccountUtil();

}
