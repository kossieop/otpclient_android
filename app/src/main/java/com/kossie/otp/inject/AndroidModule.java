package com.kossie.otp.inject;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.kossie.otp.OtpApp;
import com.kossie.otp.util.AccountUtil;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sunghun on 20/09/15.
 */
@Module
public class AndroidModule {

    private final OtpApp app;

    public AndroidModule(OtpApp app) {
        this.app = app;
    }

    @Singleton
    @Provides
    Context provideApplicationContext(){
        return this.app;
    }

    @Singleton
    @Provides
    public SharedPreferences providePreference(){
        return PreferenceManager.getDefaultSharedPreferences(this.app);
    }

    @Singleton
    @Provides
    public Gson provideGson(){
        return new Gson();
    }

    @Singleton
    @Provides
    public AccountUtil provideAccountUtil(){
        return new AccountUtil(PreferenceManager.getDefaultSharedPreferences(this.app), new Gson());
    }
}
