package com.kossie.otp;

import android.app.Application;

import com.kossie.otp.inject.AndroidModule;
import com.kossie.otp.inject.ApplicationComponent;
import com.kossie.otp.inject.DaggerApplicationComponent;

/**
 * Created by sunghun on 20/09/15.
 */
public class OtpApp extends Application {

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerApplicationComponent.builder()
                .androidModule(new AndroidModule(this))
                .build();
        component.inject(this);
    }

    public ApplicationComponent component(){
        return component;
    }
}
