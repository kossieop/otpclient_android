package com.kossie.otp.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kossie.otp.R;
import com.kossie.otp.util.PasswordUtil;

import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by sunghun on 6/09/15.
 */
public class OtpFragment extends Fragment {

    @Bind(R.id.spin_account)
    AppCompatSpinner mSpinAccount;
    @Bind(R.id.prg_progress)
    ProgressBar mProgressBar;
    @Bind(R.id.text_password)
    TextView mTextPassword;
    @Bind(R.id.btn_generate)
    Button mBtnGenerate;

    private Random mRandom = new Random();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_otp, container, false);
        ButterKnife.bind(this, view);

        initializeView(view);
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mHandler.sendEmptyMessage(3);
    }

    private void initializeView(View view) {
        mProgressBar.setMax(30);

        setupAccounts();
        mTextPassword.setText(PasswordUtil.generateOtp());
    }

    private void setupAccounts(){
        //mSpinAccount.setAdapter();
    }

    @OnClick(R.id.btn_generate)
    public void onBtnGenerateClick(View view){
        mHandler.sendEmptyMessage(3);
        mTextPassword.setText(PasswordUtil.generateOtp());
        mHandler.sendEmptyMessage(1);
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 1:
                    mProgressBar.setProgress(0);
                    mBtnGenerate.setEnabled(false);
                    mHandler.sendEmptyMessage(2);
                    break;
                case 2:
                    mProgressBar.setProgress(mProgressBar.getProgress() + 1);
                    if (mProgressBar.getProgress() < mProgressBar.getMax())
                        mHandler.sendEmptyMessageDelayed(2, 1000);
                    else
                        mHandler.sendEmptyMessage(3);
                    break;
                case 3:
                    mHandler.removeMessages(2);
                    mBtnGenerate.setEnabled(true);
                    break;
            }
        }
    };
}
