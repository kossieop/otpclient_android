/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kossie.otp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

import com.kossie.otp.OtpApp;
import com.kossie.otp.R;
import com.kossie.otp.account.AccountActivity;
import com.kossie.otp.apptour.HowItWorksActivity;
import com.kossie.otp.model.OtpAccount;
import com.kossie.otp.settings.SettingActivity;
import com.kossie.otp.util.AccountUtil;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * TODO
 */
public class MainActivity extends AppCompatActivity {

    private AccountUtil mAccountUtil;

    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @Bind(R.id.nav_view)
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setupDependencyInjection();
        setupToolbar();
        setupNavigationDrawer();


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        });

        if (savedInstanceState == null) {
            showOtp();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateNavigationItems();
    }

    /**
     *
     */
    private void setupDependencyInjection(){
        mAccountUtil = ((OtpApp)getApplication()).component().provideAccountUtil();
    }

    /**
     *
     */
    private void setupToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    /**
     *
     */
    private void setupNavigationDrawer(){
        if (navigationView != null) {
            setupDrawerContent();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sample_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent() {
        updateNavigationItems();
        navigationView.setNavigationItemSelectedListener(menuItem -> {
            menuItem.setChecked(true);
            mDrawerLayout.closeDrawers();

            switch (menuItem.getItemId()) {
                case R.id.nav_add_accounts:
                    showAccountsFragment();
                    break;
                case R.id.nav_otp:
                    showOtp();
                    break;
                case R.id.nav_how_it_works:
                    showHowItWorks();
                    break;
                case R.id.nav_settings:
                    showSettings();
                    break;
                default:
                    accountClick(menuItem);
            }

            return true;
        });
    }

    private void accountClick(MenuItem menuItem) {

    }

    private void showOtp() {
        String tag = getString(R.string.one_time_password);
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentByTag(tag);
        if (fragment == null)
            fragment = new OtpFragment();
        fm.beginTransaction()
                .replace(R.id.fragment_content, fragment, tag)
                .commit();

    }

    private void showHowItWorks() {
        Intent intent = new Intent(this, HowItWorksActivity.class);
        startActivity(intent);
    }

    private void showSettings() {
        Intent intent = new Intent(this, SettingActivity.class);
        startActivity(intent);
    }

    private void showAccountsFragment() {
        Intent intent = new Intent(this, AccountActivity.class);
        intent.putExtra(AccountActivity.EXTRA_IS_EDIT, true);
        startActivity(intent);
    }

    /**
     *
     */
    private void updateNavigationItems(){
        if (navigationView == null) return;

        final Menu menu = navigationView.getMenu();
        menu.removeItem(android.R.id.text1);

        final SubMenu subMenu = menu.addSubMenu(R.id.group2, android.R.id.text1, 0, "Accounts");

        ArrayList<OtpAccount> accounts = mAccountUtil.getAccountsFromPreference();
        for (OtpAccount account : accounts) {
            subMenu.add(account.getName());
        }

        MenuItem mi = menu.getItem(menu.size()-1);
        mi.setTitle(mi.getTitle());
    }
}
