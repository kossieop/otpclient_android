package com.kossie.otp.util;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kossie.otp.model.OtpAccount;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by sunghun on 20/09/15.
 */
public class AccountUtil {

    //@Inject
    Gson gson;
    //@Inject
    SharedPreferences preferences;
    private ArrayList<OtpAccount> accounts;

    private Type collectionType = null;

    @Inject
    public AccountUtil(SharedPreferences preferences, Gson gson) {
        this.preferences = preferences;
        this.gson = gson;
        this.accounts = new ArrayList<OtpAccount>();
        this.collectionType = new TypeToken<List<OtpAccount>>(){}.getType();
    }

    public ArrayList<OtpAccount> getAccounts() {
        return accounts;
    }

    public ArrayList<OtpAccount> getAccountsFromPreference() {
        String jsonStr = preferences.getString("accounts", "[]");

        System.out.println("#### JSON:" + jsonStr);

        accounts = gson.fromJson(jsonStr, this.collectionType);

        return accounts;
    }

    public void setAccounts(ArrayList<OtpAccount> accounts) {
        this.accounts = accounts;
    }

    public SharedPreferences getPreferences() {
        return preferences;
    }

    public void setPreferences(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    public boolean exist(OtpAccount account){
        return accounts.contains(account);
    }

    public boolean add(OtpAccount account){
        if (!accounts.add(account))
            return false;

        return save();
    }

    public boolean remove(OtpAccount account){
        if  (accounts.remove(account)){
            save();
            return true;
        }

        return false;
    }

    public boolean remove(String name){
        OtpAccount account = new OtpAccount();
        account.setName(name);
        return remove(account);
    }

    public void update(OtpAccount account){
        int position = accounts.indexOf(account);
        if (position>-1){
            accounts.set(position, account);
            save();
        }
    }

    public boolean save(){
        try {
            String jsonStr = gson.toJson(accounts, this.collectionType);
            //TODO. encryption/decryption & how to improve the performance eg) better to use SQLite?
            //Encryption ecryption = Encryption.getDefault("MyKey", "MySalt", "abc".getBytes());
//        try {
            //String ecryptedAccounts = ecryption.encrypt(jsonStr);
            SharedPreferences.Editor editor = preferences.edit();
            return editor.putString("accounts", jsonStr)
                         .commit();
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (NoSuchPaddingException e) {
//            e.printStackTrace();
//        } catch (InvalidAlgorithmParameterException e) {
//            e.printStackTrace();
//        } catch (InvalidKeyException e) {
//            e.printStackTrace();
//        } catch (InvalidKeySpecException e) {
//            e.printStackTrace();
//        } catch (BadPaddingException e) {
//            e.printStackTrace();
//        } catch (IllegalBlockSizeException e) {
//            e.printStackTrace();
//        }

        } catch (Exception e){
            e.printStackTrace();
        }

        return false;
    }
}
